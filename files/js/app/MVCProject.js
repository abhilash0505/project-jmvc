/*
Copyright (c) 2011 Abhilash Gopal

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


(function( $ ){
	function MVC(){
		// Set default properties.
		
		this.id = "My GUID";
		this.controllers = [];
		this.models = {
			cache: {},
			classes: {}
		};
		this.views = {
			cache: {},
			classes: {}
		};
		this.isRunning = false;
	};
		
		
	MVC.prototype.addClass = function( target, value ){
		var constructor = value.constructor;
		if (constructor == Function){
			var className = value.toString().match( new RegExp( "^function\\s+([^\\s\\(]+)", "i" ) )[ 1 ];
			target.classes[ className ] = value;
		} else {
			var className = value.constructor.toString().match( new RegExp( "^function\\s+([^\\s\\(]+)", "i" ) )[ 1 ];
			target.classes[ className ] = value.constructor;
			target.cache[ className ] = value;
			if (this.isRunning){
				this.initClass( value );
			}
		}
	};
		
	MVC.prototype.addController = function( controller ){
		this.controllers.push( controller );
		if (this.isRunning){
			this.initClass( controller );
		}
	};
	
	MVC.prototype.addModel = function( model ){
		this.addClass( this.models, model );
	};
	
	MVC.prototype.addView = function( view ){
		this.addClass( this.views, view );
	};
	
	MVC.prototype.getClass = function( target, className, initArguments ){	
		try{
			if (target.cache[ className ]){
				return( target.cache[ className ] );
			} else {
				var newInstance = new (target.classes[ className ])();
				target.classes[ className ].apply( newInstance, initArguments );
				return( newInstance );
			
			}
		}catch(e){
			;//Just log something here.
		}
	};
	
	
	MVC.prototype.getFromTemplate = function( template, model ){
		// Get the raw HTML from the template.
		var templateData = template.html();
		
		// Replace any data place holders with model data.
		templateData = templateData.replace(
			new RegExp( "\\$\\{([^\\}]+)\\}", "gi" ),
			function( $0, $1 ){
				if ($1 in model){
					return( model[ $1 ] );
				} else {
					return( $0 )
				}
			}
		);
		return( $( templateData ).data( "model", model ) );
	};
	
	
	MVC.prototype.getModel = function( className, initArguments ){
		return( this.getClass( this.models, className, initArguments ) );
	};
	
	
	MVC.prototype.getView = function( className, initArguments ){
		return( this.getClass( this.views, className, initArguments ) );
	};
	
	
	MVC.prototype.initClass = function( instance ){
		if (instance.init){
			instance.init();
		}
	};
		
		
	MVC.prototype.initClasses = function( classes ){
		var self = this;
		
		$.each(
			classes,
			function( index, instance ){
				self.initClass( instance );
			}
		);
	};
	
	
	MVC.prototype.initControllers = function(){
		this.initClasses( this.controllers );
	};
	
	

	MVC.prototype.initModels = function(){
		this.initClasses( this.models.cache );
	};
	
	MVC.prototype.initViews = function(){
		this.initClasses( this.views.cache );
	};
	
	MVC.prototype.log = function(){
		if (window.console && window.console.log){
			window.console.log.apply( window.console, arguments );
		} else {
			//Mainly for Sucky IE
			$.each(
				arguments,
				function( index, value ){
					$( document.body ).append( "<p>" + value.toString() + "</p>" );
				}
			);
		
		}
	};
	
	MVC.prototype.proxyCallback = function( callback ){
		var self = this;
		return(
			function(){
				return( callback.apply( self, arguments ) );
			}	
		);
	}
	

	
	
	//Begin Running the MVC
	MVC.prototype.run = function(){
		// Initialize the model.
		this.initModels();
		
		// Initialize the views.
		this.initViews();
		
		// Initialize the controllers.
		this.initControllers();
		
		// Flag that the MVC is running.
		this.isRunning = true;
	};
		
	
	
	// ----------------------------------------------------------------------- //
	// ----------------------------------------------------------------------- //
	
	

	MVC.prototype.Controller = function(){
		// ...
	};
	
	// ----------------------------------------------------------------------- //
	// ----------------------------------------------------------------------- //
	
	
	// Create a new instance of the MVC and store it in the window.
	window.MVC = new MVC();
	
	// When the DOM is ready, run the MVC.
	$(function(){
		window.MVC.run();
	});
	
	// Return a new MVC instance.
	return( window.MVC );
	
})( jQuery );
